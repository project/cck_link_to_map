<?php

/**
 * @file
 * Display field contents as a link to Google Maps.  Link opens in new window/tab.  Simplest thing that could possibly work.
 *
 * 6762 Divisadero San Francisco, CA 94117
 *    becomes:
 * <a href="http://maps.google.com/maps?q=6762 Divisadero San Francisco, CA 94117">6762 Divisadero San Francisco, CA 94117</a>
 *
 * There are no comments here because I don't understand this code.  It's cut/paste
 * CCK D6 module documentation is like WOW.
 */

define('GOOGLE_MAPS_LINK', 'http://maps.google.com/maps?q=');

function cck_link_to_map_help($section) {
    switch ($section) {
        case 'admin/modules#description':
            return t('CCK Links for Google Maps.  Use the fields text to create a link to a google map in a new window.  Simplest thing that could possibly work.');
    }
}

function cck_link_to_map_field_info() {
    return array(
    'cck_link_to_map' => array('label' => t('Link to Map')),
    );
}

function cck_link_to_map_theme() {
    return array(
    'cck_link_to_map_textfield' => array(
    'arguments' => array('element' => NULL),
    ),
    'cck_link_to_map_formatter_default' => array(
    'arguments' => array('element' => NULL),
    ),
    );
}

function cck_link_to_map_field_settings($op, $field) {
    switch ($op) {
        case 'database columns':
            if ($field['type'] == 'cck_link_to_map') {
                $columns = array(
                    'value' => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE),
                );
            }
            return $columns;
    }
}

function cck_link_to_map_field($op, &$node, $field, &$node_field, $teaser, $page) {
    switch ($op) {
        case 'validate':
            break;
    }
}

function cck_link_to_map_field_formatter_info() {
    return array(
    'default' => array(
    'label' => 'Default',
    'field types' => array('cck_link_to_map'),
    'multiple values' => CONTENT_HANDLE_CORE,
    ),
    );
}

function cck_link_to_map_field_formatter($field, $item, $formatter, $node) {
    if (!isset($item['value'])) {
        return '';
    }
    if ($field['text_processing']) {
        $text = check_markup($item['value'], $item['format'], is_null($node) || isset($node->in_preview));
    }
    else {
        $text = check_plain($item['value']);
    }
    return $text;
}

function cck_link_to_map_widget_info() {
    return array(
    'cck_link_to_map_textfield' => array(
    'label' => t('Link To Map'),
    'field types' => array('cck_link_to_map'),
    'multiple values' => CONTENT_HANDLE_CORE,
    'callbacks' => array('default value' => CONTENT_CALLBACK_DEFAULT),
    ),
    );
}

function cck_link_to_map_widget_settings($op, $widget) {
    switch ($op) {
        case 'form':
            break;
        case 'validate':
            break;
        case 'save':
            return array();
    }
}

function cck_link_to_map_widget(&$form, &$form_state, $field, $items, $delta = 0) {
    $element = array(
        '#type' => $field['widget']['type'],
        '#default_value' => isset($items[$delta]) ? $items[$delta] : NULL,
    );
    return $element;
}

function cck_link_to_map_content_is_empty($item, $field) {
    return FALSE;
}

function cck_link_to_map_elements() {
    return array(
    'cck_link_to_map_textfield' => array(
    '#input' => TRUE,
    '#columns' => array('value'), '#delta' => 0,
    '#process' => array('cck_link_to_map_textfield_process'),
    '#autocomplete_path' => FALSE,
    ),
    );
}

function theme_cck_link_to_map_textfield($element) {
    return $element['#children'];
}

function cck_link_to_map_textfield_process($element, $edit, $form_state, $form) {
    $field = $form['#field_info'][$element['#field_name']];
    $field_key = $element['#columns'][0];
    $delta = $element['#delta'];
    $element[$field_key] = array(
        '#type' => 'textfield',
        '#default_value' => isset($element['#value'][$field_key]) ? $element['#value'][$field_key] : NULL,
        '#autocomplete_path' => FALSE,
        // The following values were set by the content module and need
        // to be passed down to the nested element.
        '#title' => $element['#title'],
        '#description' => $element['#description'],
        '#required' => $element['#required'],
        '#field_name' => $element['#field_name'],
        '#type_name' => $element['#type_name'],
        '#delta' => $element['#delta'],
        '#columns' => $element['#columns'],
    );

    if (!empty($field['max_length'])) {
        $element[$field_key]['#maxlength'] = $field['max_length'];
    }
    if (!empty($field['text_processing'])) {
        $filter_key = $element['#columns'][1];
        $format = isset($element['#value'][$filter_key]) ? $element['#value'][$filter_key] : FILTER_FORMAT_DEFAULT;
        $parents = array_merge($element['#parents'] , array($filter_key));
        $element[$filter_key] = filter_form($format, 1, $parents);
    }

    // Used so that hook_field('validate') knows where to flag an error.
    $element['_error_element'] = array(
        '#type' => 'value',
        '#value' => implode('][', array_merge($element['#parents'], array($field_key))),
    );

    return $element;
}

function theme_cck_link_to_map_formatter_default($element) {
    $text = $element['#item']['value'];
    $options['attributes']['target'] = '_blank';
    $text = l($text, GOOGLE_MAPS_LINK . $text, $options);
    return $text;
}
